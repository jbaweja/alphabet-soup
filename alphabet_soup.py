import re

def parse_file():
    with open('input3.txt', 'r') as file:
        lines = [line.strip() for line in file.readlines()]

    grid_size = [int(x) for x in lines[0].split('x')]
    num_rows = grid_size[0]
    grid = [line.split() for line in lines[1:num_rows + 1]]
    words = lines[num_rows + 1:]

    return grid, words

def word_search(grid, word):
    rows, cols = len(grid), len(grid[0])

    for row in range(rows):
        for col in range(cols):
            for row_dir, col_dir in [(1, 0), (0, 1), (1, 1), (-1, 0), (0, -1), (-1, -1), (1, -1), (-1, 1)]:
                match = True
                for i in range(len(word)):
                    r, c = row + i * row_dir, col + i * col_dir
                    if r < 0 or r >= rows or c < 0 or c >= cols or grid[r][c] != word[i]:
                        match = False
                        break

                if match:
                    first = f"{row}:{col}"
                    last = f"{row + (len(word) - 1) * row_dir}:{col + (len(word) - 1) * col_dir}"
                    return f"{word} {first} {last}"

    return f"{word} not found!"

grid, words = parse_file()

for word in words:
    result = word_search(grid, word)
    print(result)

